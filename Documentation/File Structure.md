# File Structure Standard V1.0
#### Author: David Collins-Cubitt

Listed is the main file structure standard for the OWF.

 Documentation (All the documentation files)
 - Index.php
 - Licence.php
 - Buildinfo.txt
 - Readme.md // Github Description (and basic use)

 Config
    - settings.php

 Content
    Images
    - Home.php
    - Page1.php
    - Page2.php
    - Footer.php

 Libarys (to add modules just paste them in here)

 Themes
    Default
        - Layout.php
        - Style.php
        - Assets (Images, Logos, extras)
