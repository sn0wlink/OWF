<?php 

echo "

body {
    font-family: arial, verdana;
}

img.headerlogo {
    position: absolute;
    height: 100px;
    left:0;
    right: 0;
    margin: auto;
    float: top;
}

div.headertext {
    position: absolute;
    left:0;
    right: 0;
    margin: auto;
    text-align: center;
}

div.content {
    left:0;
    right: 0;
    margin: auto;
}

div.menu {
    top: 120px;
    background-color: #000000;
    width: 900px;
    position: absolute;
    left:0;
    right: 0;
    margin: auto;
}

div.menuitem {
    font-size: 10pt;
    font-weight: bold;
    color: #ffffff;
    float:right;
    padding: 10px;
}

div.footer {
    width: 100%;
    background-color: #000000;
    color: #ffffff;
    bottom:0;
    padding: 10px;
    position: fixed;
    left:0;
    right: 0;
    margin: auto;
    text-align: center;
    font-size: 9pt;
}

";
?>