<?php

/*

    Welcome to the Open Web Framework
    A simple, fast and light framework with no reliance on a backend database

    Author: David Collins-Cubitt
    Website: sn0wlink.com
    Prodject: github.com/sn0wlink/OWF

*/

// Setup Files
    include ("config/settings.php");

    // Pull modules from libraries
    foreach (glob("libraries/*",GLOB_ONLYDIR) as $file) {
        include ("$file/module.php");
    }

// Build page template
include ("themes/$theme/layout.html");

?>