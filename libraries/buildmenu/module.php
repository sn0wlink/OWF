<?php

function buildmenu () {

    // Define menu item for CSS
    echo "
        <div class='menubar'>
    ";

    // Pull in page names
    foreach (glob("content/pages/*") as $pagename) {
        $pagename = trim ($pagename, "content/pages/");
        $pagename = trim ($pagename, ".php");

        echo "
        <div class='menuitem'>
            $pagename 
        </div>
        ";
    }

    echo "
        </div>
    ";
}

?>