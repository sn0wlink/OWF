<?php 

// Display Header Logo on Page.
function DisplayLogoText() {

    // Include headerlogo variable from settings.php
    global $DisplayLogoText;
    global $sitename;

    if ($DisplayLogoText == TRUE) {
        echo "<div class='headertext'><h1>$sitename</h1></div>";
    }
}

?>