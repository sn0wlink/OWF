# OWF - Open Web Framework
The Open Web Framework - A simple open source dynamic website generation engine.

Designed for web developers to easily customise and deploy simple websites for customers needs for fast turnover.

Developer Url: http://sn0wlink.com

Licence: GPLv3
