<?php

/*
    OWF CONFIGURATION PAGE
    - Please change to suit your needs -
*/

// Site Name
$sitename = "Open Web Framework";

// Theme Folder Name
$theme = "Default";

// SEO Details
$keywords = "Open Web Framework Standard";
$sitedescription = "A lightweight open-source CMS for building websites";

// Debug Information
$DisplayErrors = True; // True or False

// Display Site Logo
$DisplayLogo = False; // True or False

// Display Site Logo
$DisplayLogoText = True; // True or False

// Homepage Filename
$homepagefile = "Home.php"; // Default is Home.php

?>